//
//  RecentChat.swift
//  Whatsapp Clone
//
//  Created by Wasiq Tayyab on 26/07/2021.
//

import Foundation
import FirebaseFirestoreSwift

struct RecentChat: Codable {
    
    var id = ""
    var chatRoomId = ""
    var senderId = ""
    var senderName = ""
    var receiverId = ""
    var receiverName = ""
    @ServerTimestamp var date = Date()
    var memberId: [String] = [""]
    var lastMessage = ""
    var unreadCounter = 0
    var avatarLink = ""
    
}
