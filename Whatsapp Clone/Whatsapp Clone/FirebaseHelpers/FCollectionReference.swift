//
//  FCollectionReference.swift
//  Whatsapp Clone
//
//  Created by Wasiq Tayyab on 13/07/2021.
//

import Foundation
import FirebaseFirestore

enum FCollectionReference: String {
    case User
    case Recent
    case Messages
    case Typing
    case Channel
    
}

func FirebaseReference(_ collectionReference: FCollectionReference)-> CollectionReference{
    return Firestore.firestore().collection(collectionReference.rawValue)
}
