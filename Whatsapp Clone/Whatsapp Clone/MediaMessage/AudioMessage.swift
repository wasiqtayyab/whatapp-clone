//
//  AudioMessage.swift
//  Whatsapp Clone
//
//  Created by Wasiq Tayyab on 29/07/2021.
//

import Foundation
import MessageKit


class AudioMessage: NSObject, AudioItem {
    
    var url: URL
    var duration: Float
    var size: CGSize
    
    init(duration: Float){
        
        self.url = URL(fileURLWithPath: "")
        self.size = CGSize(width: 160, height: 35)
        self.duration = duration
        
    }
}
