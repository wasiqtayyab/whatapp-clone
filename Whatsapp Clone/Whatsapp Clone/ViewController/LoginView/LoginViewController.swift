//
//  ViewController.swift
//  Whatsapp Clone
//
//  Created by Wasiq Tayyab on 12/07/2021.
//

import UIKit
import ProgressHUD
class LoginViewController: UIViewController {
    
    //MARK:- IBOutlets
    //Labels
    
    @IBOutlet weak var emailLabelOutlet: UILabel!
    @IBOutlet weak var passwordLabelOutlet: UILabel!
    @IBOutlet weak var repeatPasswordLabelOutlet: UILabel!
    @IBOutlet weak var signUpLabel: UILabel!
    
    //Textfield
    
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var repeatPasswordTextfield: UITextField!
    
    //Buttons
    
    
    @IBOutlet weak var loginButtonOutlet: UIButton!
    @IBOutlet weak var forgetPasswordButtonOutlet: UIButton!
    @IBOutlet weak var resendEmailButtonOutlet: UIButton!
    @IBOutlet weak var signUpButtonOutlet: UIButton!
    
    //Views

    @IBOutlet weak var repeatPasswordLineView: UIView!
    
    //MARK:- Vars
    
    var isLogin = true
    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUIFor(login: true)
        setupTextFieldDelegates()
        setupBackgroundTap()
        
        
    }
    
    //MARK:- IBActions
    
    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        if isDataInputedFor(type: isLogin ? "login" : "register"){
            
            isLogin ? loginUser() : registerUser()
            
        }else {
            ProgressHUD.showFailed("All Fields are required")
            
        }
    }
    
    
    @IBAction func forgetPasswordButtonPressed(_ sender: UIButton) {
        
        if isDataInputedFor(type: "password"){
          
           resetPassword()
            
        }else {
            ProgressHUD.showFailed("Email is required.")
            
        }
    }
    
    
    
    @IBAction func resendEmailButtonPressed(_ sender: UIButton) {
        if isDataInputedFor(type: "password"){
            resendVerficationEmail()
        }else {
            ProgressHUD.showFailed("Email is required.")
            
        }
    }
    
    
    
    @IBAction func signUpButtonPressed(_ sender: UIButton) {
        updateUIFor(login: sender.titleLabel?.text == "Login")
        isLogin.toggle()
    }
    
    //MARK:- Setup
    
    private func setupTextFieldDelegates(){
        emailTextfield.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        passwordTextfield.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        repeatPasswordTextfield.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField){
        updatePlaceholderLabels(textField: textField)
    }
    
    private func setupBackgroundTap(){
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(backgroundTap))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func backgroundTap(){
        view.endEditing(false)
        
    }
    
    //MARK:- Animations
    
    private func updateUIFor(login: Bool){
        loginButtonOutlet.setImage(UIImage(named: login ? "btnLogin" : "registerBtn"), for: .normal)
        signUpButtonOutlet.setTitle(login ? "SignUp" : "Login", for: .normal)
        signUpLabel.text = login ? "Don't have an account?" : "Have an account?"
        
        UIView.animate(withDuration: 0.5) {
            self.repeatPasswordLabelOutlet.isHidden = login
            self.repeatPasswordTextfield.isHidden = login
            self.repeatPasswordLineView.isHidden = login
        }
    }
    
    private func updatePlaceholderLabels(textField: UITextField){
        switch textField {
        case emailTextfield:
            emailLabelOutlet.text = textField.hasText ? "Email": ""
        case passwordTextfield:
            passwordLabelOutlet.text = textField.hasText ? "Password": ""
            
        default:
            repeatPasswordLabelOutlet.text = textField.hasText ? "Repeat Password": ""
        }
    }
    
    //MARK:- Helpers
    
    private func isDataInputedFor(type: String) -> Bool {
        
        switch type {
        case "login":
            return emailTextfield.text != "" && passwordTextfield.text != ""
        case "registration":
            return emailTextfield.text != "" && passwordTextfield.text != "" && repeatPasswordTextfield.text != ""
        default:
            return emailTextfield.text != ""
        }
        
        
    }
    
    private func loginUser(){
        
        FirebaseUserListener.shared.loginUserWithEmail(email: emailTextfield.text!, password: passwordTextfield.text!) { (error, isEmailVerified) in
            if error == nil {
                if isEmailVerified {
                    self.goToApp()
                }else {
                    ProgressHUD.showFailed("Please verify email.")
                    self.resendEmailButtonOutlet.isHidden = false
                }
            }else {
                ProgressHUD.showFailed(error?.localizedDescription)
            }
        }
        
        
    }
    
    private func registerUser(){
        if passwordTextfield.text == repeatPasswordTextfield.text {
            FirebaseUserListener.shared.registerUserWith(email: emailTextfield.text!, password: passwordTextfield.text!) { (error) in
                if error == nil {
                    ProgressHUD.showSuccess("Verification email sent.")
                    self.resendEmailButtonOutlet.isHidden = false
                }else {
                    ProgressHUD.showFailed(error?.localizedDescription)
                    
                }
            }
        }else {
            ProgressHUD.showError("The passwords don't match")
        }
    }
    
    private func resetPassword(){
        FirebaseUserListener.shared.resetPasswordFor(email: emailTextfield.text!) { (error) in
            if error == nil {
                ProgressHUD.showSuccess("Resent link sent to email")
            }else {
                ProgressHUD.showFailed(error?.localizedDescription)
            }
        }
        
    }
    
    private func resendVerficationEmail(){
        FirebaseUserListener.shared.resendVerificationEmail(email: emailTextfield.text!) { (error) in
            if error == nil {
                ProgressHUD.showSuccess("Now verification email sent.")
            }else {
                ProgressHUD.showFailed(error?.localizedDescription)
            }
        }
    }
    
    //MARK:- Navigation
    
    private func goToApp(){
        
        let mainView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MainView")as! UITabBarController
        mainView.modalPresentationStyle = .fullScreen
        self.present(mainView, animated: true, completion: nil)
    }
    
    
    
}

