//
//  ChannelTableViewController.swift
//  Whatsapp Clone
//
//  Created by Wasiq Tayyab on 30/07/2021.
//

import UIKit


protocol ChannelTableViewControllerDelegate {
    func didClickFollow()
}


class ChannelDetailTableViewController: UITableViewController {
    
    //MARK:- IBActions
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var memberLabel: UILabel!
    @IBOutlet weak var aboutTextView: UITextView!
    
    //MARK:- Vars
    
    var channel: Channel!
    var delegate: ChannelTableViewControllerDelegate?
    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.largeTitleDisplayMode = .never
        tableView.tableFooterView = UIView()
        
        showChannelData()
        configureRightBarButton()
    }
    
    //MARK:- Configure
    
    private func showChannelData(){
        
        self.title = channel.name
        
        nameLabel.text = channel.name
        memberLabel.text = "\(channel.memberIds.count) Members"
        aboutTextView.text = channel.aboutChannel
        setAvatar(avatarLink: channel.avatarLink)
        
    }
    
    private func configureRightBarButton(){
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Follow", style: .plain, target: self, action: #selector(followChannel))
        
    }

    private func setAvatar(avatarLink: String){
        
        if avatarLink != "" {
            
            FileStorage.downloadImage(imageUrl: avatarLink) { (avatarImage) in
                DispatchQueue.main.async {
                    self.avatarImageView.image = avatarImage != nil ? avatarImage?.circleMasked : UIImage(named: "avatar")
                    
                }
            }
        }else {
            self.avatarImageView.image = UIImage(named: "avatar")
        }
        
        
    }
    
    //MARK:- Action
    
    @objc func followChannel(){
        
        channel.memberIds.append(User.currentId)
        FirebaseChannelListener.shared.saveChannel(channel)
        delegate?.didClickFollow()
        self.navigationController?.popViewController(animated: true)
        
        
    }
    
    
    
    
    
}
